from scipy.signal import convolve2d
import numpy as np



def compute_gradients(R_directions, G_directions, B_directions, L):
    """
    This function will compute the gradients of the reconstructed channels, in each direction and in the YUV space
    Inputs : 
        - R_directions, G_directions, B_directions : the reconstructed channels
        - L : the size of the neighborhood on which to compute the gradients
    Outputs :
        - gradients : the gradients of the reconstructed channels in each direction
    """

    Y = 0.299 * R_directions + 0.587 * G_directions + 0.114 * B_directions
    U = R_directions - Y
    V = B_directions - Y

    # U_roll_north = np.roll(U, -L, axis=0)
    # U_roll_south = np.roll(U, L, axis=0)
    # U_roll_east = np.roll(U, -L, axis=1)
    # U_roll_west = np.roll(U, L, axis=1)

    # V_roll_north = np.roll(V, -L, axis=0)
    # V_roll_south = np.roll(V, L, axis=0)
    # V_roll_east = np.roll(V, -L, axis=1)
    # V_roll_west = np.roll(V, L, axis=1)

    H, W = U.shape[0], U.shape[1]

    gradients = np.zeros((H, W, 4))

    for height in range(U.shape[0]):
        for width in range(U.shape[1]):
            somme_u = [0, 0, 0, 0]
            somme_v = [0, 0, 0, 0]
            for l in range(1, L+1):
                somme_u[0] += (U[height, width, 0] -
                               U[(height-l) % H, width, 0])**2
                somme_u[1] += (U[height, width, 1] -
                               U[(height+l) % H, width, 1])**2
                somme_u[2] += (U[height, width, 2] -
                               U[height, (width-l) % W, 2])**2
                somme_u[3] += (U[height, width, 3] -
                               U[height, (width+l) % W, 3])**2

                somme_v[0] += (V[height, width, 0] -
                               V[(height-l) % H, width, 0])**2
                somme_v[1] += (V[height, width, 1] -
                               V[(height+l) % H, width, 1])**2
                somme_v[2] += (V[height, width, 2] -
                               V[height, (width-l) % W, 2])**2
                somme_v[3] += (V[height, width, 3] -
                               V[height, (width+l) % W, 3])**2

            gradients[height, width] = 1/L * \
                (np.sqrt(somme_u) + np.sqrt(somme_v))

    return gradients


def local_int(CFA_image, beta, L, epsilon, cfa_mask):
    """
    This function will compute the reconstructed image, using directional interpolation, and will then select the best direction for each pixel.
    Inputs : 
        - CFA_image : the three channels of the image (CFA)
        - beta : the parameter of intercorrelation between the channels
        - L : the size of the neighborhood on which to test the direction
        - epsilon : the threshold for the selection of the direction

    Outputs :
        - R, G, B : the reconstructed channels
    """


    G_directions = np.zeros((CFA_image.shape[0], CFA_image.shape[1], 4))
    R_directions = np.zeros((CFA_image.shape[0], CFA_image.shape[1], 4))
    B_directions = np.zeros((CFA_image.shape[0], CFA_image.shape[1], 4))
    RG_directions = np.zeros((CFA_image.shape[0], CFA_image.shape[1], 4))
    BG_directions = np.zeros((CFA_image.shape[0], CFA_image.shape[1], 4))

    # Directionnal interpolation of green channel

    G_directions[:, :, 0] = CFA_image[:, :, 1] + \
        np.roll(CFA_image[:, :, 1], -1, axis=0) + \
        beta/2 * (CFA_image[:, :, 0] - np.roll(CFA_image[:, :, 0], -2, axis=0))

    G_directions[:, :, 1] = CFA_image[:, :, 1] + \
        np.roll(CFA_image[:, :, 1], 1, axis=0) + \
        beta/2 * (CFA_image[:, :, 0] - np.roll(CFA_image[:, :, 0], 2, axis=0))

    G_directions[:, :, 2] = CFA_image[:, :, 1] + \
        np.roll(CFA_image[:, :, 1], -1, axis=1) + \
        beta/2 * (CFA_image[:, :, 0] - np.roll(CFA_image[:, :, 0], -2, axis=1))

    G_directions[:, :, 3] = CFA_image[:, :, 1] + \
        np.roll(CFA_image[:, :, 1], 1, axis=1) + \
        beta/2 * (CFA_image[:, :, 0] - np.roll(CFA_image[:, :, 0], 2, axis=1))

    # Bilinear interpolation of red and blue channels

    R_directions[:, :, 0] += CFA_image[:, :, 0]
    R_directions[:, :, 1] += CFA_image[:, :, 0]
    R_directions[:, :, 2] += CFA_image[:, :, 0]
    R_directions[:, :, 3] += CFA_image[:, :, 0]

    B_directions[:, :, 0] += CFA_image[:, :, 2]
    B_directions[:, :, 1] += CFA_image[:, :, 2]
    B_directions[:, :, 2] += CFA_image[:, :, 2]
    B_directions[:, :, 3] += CFA_image[:, :, 2]

    RG_directions[:, :, 0] += RG_directions[:, :, 0] - \
        beta*G_directions[:, :, 0]
    RG_directions[:, :, 1] += RG_directions[:, :, 1] - \
        beta*G_directions[:, :, 1]
    RG_directions[:, :, 2] += RG_directions[:, :, 2] - \
        beta*G_directions[:, :, 2]
    RG_directions[:, :, 3] += RG_directions[:, :, 3] - \
        beta*G_directions[:, :, 3]

    BG_directions[:, :, 0] += BG_directions[:, :, 0] - \
        beta*G_directions[:, :, 0]
    BG_directions[:, :, 1] += BG_directions[:, :, 1] - \
        beta*G_directions[:, :, 1]
    BG_directions[:, :, 2] += BG_directions[:, :, 2] - \
        beta*G_directions[:, :, 2]
    BG_directions[:, :, 3] += BG_directions[:, :, 3] - \
        beta*G_directions[:, :, 3]

    # Bi-linear interpolation

    kernel = 1 / 4 * np.array([
        [1, 2, 1],
        [2, 4, 2],
        [1, 2, 1]
    ])

    R_directions[:, :, 0] = convolve2d(
        R_directions[:, :, 0], kernel, mode='same')
    R_directions[:, :, 1] = convolve2d(
        R_directions[:, :, 1], kernel, mode='same')
    R_directions[:, :, 2] = convolve2d(
        R_directions[:, :, 2], kernel, mode='same')
    R_directions[:, :, 3] = convolve2d(
        R_directions[:, :, 3], kernel, mode='same')

    B_directions[:, :, 0] = convolve2d(
        B_directions[:, :, 0], kernel, mode='same')
    B_directions[:, :, 1] = convolve2d(
        B_directions[:, :, 1], kernel, mode='same')
    B_directions[:, :, 2] = convolve2d(
        B_directions[:, :, 2], kernel, mode='same')
    B_directions[:, :, 3] = convolve2d(
        B_directions[:, :, 3], kernel, mode='same')

    R_directions += beta * G_directions
    B_directions += beta * G_directions

    gradients = compute_gradients(R_directions, G_directions, B_directions, L)

    inv_gradients = 1/(gradients+epsilon)

    sommme_gradients = np.sum(inv_gradients, axis=2)

    inv_gradients[:, :, 0] = inv_gradients[:, :, 0] / sommme_gradients
    inv_gradients[:, :, 1] = inv_gradients[:, :, 1] / sommme_gradients
    inv_gradients[:, :, 2] = inv_gradients[:, :, 2] / sommme_gradients
    inv_gradients[:, :, 3] = inv_gradients[:, :, 3] / sommme_gradients

    R_1 = np.sum(R_directions * inv_gradients, axis=2)
    G_1 = np.sum(G_directions * inv_gradients, axis=2)
    B_1 = np.sum(B_directions * inv_gradients, axis=2)

    return R_1, G_1, B_1