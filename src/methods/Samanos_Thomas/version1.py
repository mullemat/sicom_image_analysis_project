import numpy as np
from skimage.transform import resize

def upscale(img: np.ndarray, shape: tuple, pas: int) -> np.ndarray:
    """Upscales the acquisition to the shape of the desired reconstruction.

    Args:
        img (np.ndarray): mosaique image in smaller size.
        shape (tuple): Shape of the output.

    Returns:
        np.ndarray: Upscaled image.
    """
    return resize(img, shape, anti_aliasing=True)

def bayer_reconstruction(y : np.ndarray, input_shape : tuple) -> np.ndarray:
    op_shape = (input_shape[0]//2,input_shape[1]//2,3)
    op = np.zeros(op_shape)
    conv = np.zeros((2,2,3))
    liste = [(0,1,0),(0,0,1),(1,1,1),(1,0,2)]
    for el in liste :
        conv[el]=1
        if el[2]==1:
            conv[el]=1/2
    for i in range (3):
        op[:,:,i] = conv2d(y,conv[:,:,i], 2)
    op1 = upscale(op,(input_shape[0],input_shape[1],3),2)
    return op1

def quad_bayer_reconstruction(y : np.ndarray, input_shape : tuple) -> np.ndarray:
    op_shape = (input_shape[0]//4,input_shape[1]//4,3)
    op = np.zeros(op_shape)
    conv = np.zeros((4,4,3))
    liste = [(2,0,2),(3,0,2),(2,1,2),(3,1,2),(0,0,1),(1,0,1),(0,1,1),(1,1,1),(2,2,1),(2,3,1),(3,2,1),(3,3,1),(0,2,0),(0,3,0),(1,2,0),(1,3,0)]
    for el in liste :
        conv[el]=1/4
        if el[2]==1:
            conv[el]=1/8
    for i in range (3):
        op[:,:,i] = conv2d(y,conv[:,:,i], 4)
    op1 = upscale(op,(input_shape[0],input_shape[1],3),4)
    return op1

def conv2d(img : np.ndarray, conv : np.ndarray, pas :int):
    y_shape = (img.shape[0], img.shape[1])
    op = np.zeros((y_shape[0]//pas,y_shape[1]//pas))
    for i in range(0,y_shape[0],pas):
        for j in range(0,y_shape[1],pas):
            op[i//pas,j//pas] = np.sum(img[i:i+pas,j:j+pas]*conv)
    return op
