import numpy as np
from scipy.ndimage import gaussian_filter

def lmmse_color_smooth_demosaicking(op, y, sigma_spatial=1.0, sigma_color=0.02):
    z = op.adjoint(y)

    if op.cfa == 'bayer':
       
        z[:, :, 1] = gaussian_filter(z[:, :, 1], sigma=sigma_spatial)

        
        z_smoothed = np.empty_like(z)
        for i in range(3):
            z_smoothed[:, :, i] = gaussian_filter(z[:, :, i], sigma=sigma_color)

        res_red = z_smoothed[:, :, 0] + 0.5 * (z[:, :, 1] - z_smoothed[:, :, 0])
        res_blue = z_smoothed[:, :, 2] + 0.5 * (z[:, :, 1] - z_smoothed[:, :, 2])
        res_green = z[:, :, 1]

        res_red = np.clip(res_red, 0, 1)
        res_blue = np.clip(res_blue, 0, 1)

        demosaicked_image = np.stack([res_red, res_green, res_blue], axis=-1)

    else:
       
        demosaicked_image = np.empty(op.input_shape)

    return demosaicked_image

