# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 07:38:51 2024

@author: etudiant
"""
import numpy as np
from scipy.signal import convolve2d
from src.forward_model import CFA
from scipy.ndimage import convolve


def malvar_demosaicing(op: CFA, y: np.ndarray) -> np.ndarray:
    """
    Performs a malvar interpolation.

    Args:
        op (CFA): CFA operator.
        y (np.ndarray): Mosaicked image.

    Returns:
        np.ndarray: Demosaicked image.
    """
    z = op.adjoint(y)
    # Definnig Masks
    R_m = op.mask[:, :, 0]
    G_m = op.mask[:, :, 1]
    B_m = op.mask[:, :, 2]

    GR_GB = np.array([
        [0.0, 0.0, -1.0, 0.0, 0.0],
        [0.0, 0.0, 2.0, 0.0, 0.0],
        [-1.0, 2.0, 4.0, 2.0, -1.0],
        [0.0, 0.0, 2.0, 0.0, 0.0],
        [0.0, 0.0, -1.0, 0.0, 0.0],
    ]) / 8

    Rg_RB_Bg_BR = np.array(
        [
            [0.0, 0.0, 0.5, 0.0, 0.0],
            [0.0, -1.0, 0.0, -1.0, 0.0],
            [-1.0, 4.0, 5.0, 4.0, -1.0],
            [0.0, -1.0, 0.0, -1.0, 0.0],
            [0.0, 0.0, 0.5, 0.0, 0.0],
        ]) / 8

    Rg_BR_Bg_RB = np.transpose(Rg_RB_Bg_BR)

    Rb_BB_Br_RR = np.array(
        [
            [0.0, 0.0, -1.5, 0.0, 0.0],
            [0.0, 2.0, 0.0, 2.0, 0.0],
            [-1.5, 0.0, 6.0, 0.0, -1.5],
            [0.0, 2.0, 0.0, 2.0, 0.0],
            [0.0, 0.0, -1.5, 0.0, 0.0],
        ]) / 8

    R = y * R_m
    G = y * G_m
    B = y * B_m

    del G_m

    G = np.where(np.logical_or(R_m == 1, B_m == 1), convolve(y, GR_GB), G)

    RBg_RBBR = convolve(y, Rg_RB_Bg_BR)
    RBg_BRRB = convolve(y, Rg_BR_Bg_RB)
    RBgr_BBRR = convolve(y, Rb_BB_Br_RR)

    del GR_GB, Rg_RB_Bg_BR, Rg_BR_Bg_RB, Rb_BB_Br_RR

    # Red rows.
    R_r = np.transpose(np.any(R_m == 1, axis=1)[None]) * np.ones(R.shape)
    # Red columns.
    R_c = np.any(R_m == 1, axis=0)[None] * np.ones(R.shape)
    # Blue rows.
    B_r = np.transpose(np.any(B_m == 1, axis=1)[None]) * np.ones(B.shape)
    # Blue columns
    B_c = np.any(B_m == 1, axis=0)[None] * np.ones(B.shape)

    del R_m, B_m

    R = np.where(np.logical_and(R_r == 1, B_c == 1), RBg_RBBR, R)
    R = np.where(np.logical_and(B_r == 1, R_c == 1), RBg_BRRB, R)

    B = np.where(np.logical_and(B_r == 1, R_c == 1), RBg_RBBR, B)
    B = np.where(np.logical_and(R_r == 1, B_c == 1), RBg_BRRB, B)

    R = np.where(np.logical_and(B_r == 1, B_c == 1), RBgr_BBRR, R)
    B = np.where(np.logical_and(R_r == 1, R_c == 1), RBgr_BBRR, B)

    del RBg_RBBR, RBg_BRRB, RBgr_BBRR, R_r, R_c, B_r, B_c
    # Combine channels
    return np.clip(np.stack((R, G, B), axis=-1), 0, 1)
