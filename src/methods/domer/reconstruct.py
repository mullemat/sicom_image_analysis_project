#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 19:35:12 2024

@author: stani1
"""

"""The main file for the baseline reconstruction.
This file should NOT be modified.
"""


import numpy as np
from PIL import Image
import subprocess
import os


from src.forward_model import CFA
from src.methods.baseline.demo_reconstruction import naive_interpolation


def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """
    current_directory = os.getcwd() #we take the current directory to be sure we will have the correct one
    input_path='src/methods/domer/image-colorization/input/0.jpg'
    input_path = os.path.join(current_directory, input_path)#location to store the image
    output_path='src/methods/domer/image-colorization/output/0.jpg'
    output_path = os.path.join(current_directory, output_path)
    # venv_path='src/methods/domer/image-colorization/image-colorization/bin/activate'
    # venv_path = os.path.join(current_directory, venv_path) #activation of the venv, but no need for a lot of versions of libraries
    y2=Image.fromarray(y)
    y2=y2.convert('L')
    y2.save(input_path)
    resolution=np.shape(y)[0]
    # activation_venv = ['source ', venv_path]
    # subprocess.run(activation_venv, shell=True)
    n=y2.size[0]
    # for i in range (n) :
    #     for j in range (n) :
    #         y2.putpixel((i,j),y.getpixel((i,j))*255)
    command = [
        'python', 'src/methods/domer/image-colorization/colorize.py',
        '-i', input_path,
        '-o', output_path,
        '-r', str(resolution)
    ]
    subprocess.run(command) #calling of the NN with the location of the input and output and the resolution
    # deactivation_venv = f"deactivate"
    # subprocess.run(deactivation_venv, shell=True)
    res=Image.open(output_path) #We take the result of the NN
    res=np.asarray(res)#reseting the result in a numpy array

    return res


