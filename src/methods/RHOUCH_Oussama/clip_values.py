import numpy as np

def clip_values(img: np.ndarray) -> np.ndarray:
    """Clip values of an image between 0 and 1
    
    Args:
        img (np.ndarray): image to clip
        
    Returns:
        np.ndarray: clipped image
    """
    # Clip values between 0 and 1
    img[img < 0] = 0
    img[img > 1] = 1 
    return img
