"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np

from src.forward_model import CFA


def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """
    # Performing the reconstruction.
    # TODO
    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA(cfa, input_shape)
    z = op.adjoint(y)
    interpolated_image = nearest_neighbor_interpolation(z)
    return interpolated_image


import numpy as np






def nearest_neighbor_interpolation(image):
    interpolated_image = np.zeros_like(image)
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            for k in range(3):
                if (image[i, j,k] == 0):  
                    closest_pixel = find_closest_nonzero_pixel(image[:,:,k], i, j)
                    interpolated_image[i, j,k] = closest_pixel
                else:
                    interpolated_image[i, j,k] = image[i, j,k]  
    return interpolated_image


def find_closest_nonzero_pixel(image, i, j):
    m, n = image.shape
    nb_pixel = 1
    while True:
        for x in range(max(0, i - nb_pixel), min(m, i + nb_pixel + 1)):
            for y in range(max(0, j - nb_pixel), min(n, j + nb_pixel + 1)):
                if np.any(image[x, y] != 0):
                    return image[x, y]
        nb_pixel += 1




# Afficher ou enregistrer l'image interpolée

####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
