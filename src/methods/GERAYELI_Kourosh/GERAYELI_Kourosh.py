
# Importing libraries
import os
import colour
from colour_demosaicing import (
    demosaicing_CFA_Bayer_bilinear,
    demosaicing_CFA_Bayer_Malvar2004,
    demosaicing_CFA_Bayer_Menon2007,
    mosaicing_CFA_Bayer)

from src.utils import psnr,ssim

# Image path
image_pathes = ['images/img_1.png','images/img_2.png','images/img_3.png','images/img_4.png']

for i in image_pathes:
    LIGHTHOUSE_IMAGE = colour.io.read_image(i)
    img = LIGHTHOUSE_IMAGE
    colour.plotting.plot_image(
        colour.cctf_encoding(LIGHTHOUSE_IMAGE))


    # Mosaicing
    CFA = mosaicing_CFA_Bayer(LIGHTHOUSE_IMAGE)

    colour.plotting.plot_image(
        colour.cctf_encoding(CFA),
        text_kwargs={'text': 'Lighthouse - CFA - RGGB'})

    colour.plotting.plot_image(
        colour.cctf_encoding(mosaicing_CFA_Bayer(LIGHTHOUSE_IMAGE, 'BGGR')), 
        text_kwargs={'text': 'Lighthouse - CFA - BGGR'});


    # Demosaicing bilinear
    colour.plotting.plot_image(
        colour.cctf_encoding(demosaicing_CFA_Bayer_bilinear(CFA)), 
        text_kwargs={'text': 'Demosaicing - Bilinear'});

    recons_bilinear = colour.cctf_encoding(demosaicing_CFA_Bayer_bilinear(CFA))


    # demosaicing Malvar
    recons_malvar = colour.cctf_encoding(demosaicing_CFA_Bayer_Malvar2004(CFA))
    colour.plotting.plot_image(
        colour.cctf_encoding(demosaicing_CFA_Bayer_Malvar2004(CFA)), 
        text_kwargs={'text': 'Demosaicing - Malvar (2004)'});

    # demosaicing Menon
    recons_menon = colour.cctf_encoding(demosaicing_CFA_Bayer_Menon2007(CFA))
    colour.plotting.plot_image(
        colour.cctf_encoding(demosaicing_CFA_Bayer_Menon2007(CFA)), 
        text_kwargs={'text': 'Demosaicing - Menon (2007)'});


    print('bilinear : ')
    print(f'PSNR: {psnr(img, recons_bilinear):.2f}')
    print(f'SSIM: {ssim(img, recons_bilinear):.4f}')

    print('Malvar : ')
    print(f'PSNR: {psnr(img, recons_malvar):.2f}')
    print(f'SSIM: {ssim(img, recons_malvar):.4f}')

    print('Menon')
    print(f'PSNR: {psnr(img, recons_menon):.2f}')
    print(f'SSIM: {ssim(img, recons_menon):.4f}')